function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  it('throws an Error beacuse name size is 0', () => {
    const nameVerification = () => {
      const e = createUser('','','','');
    };
    expect(nameVerification).toThrow();
  })

  it('throws an Error beacuse email is not valid', () => {
    const emailVerification = () => {
      const e = createUser('Beatriz','biamarougmailcom','','');
    };
    expect(emailVerification).toThrow();
  })

  it('throws an Error because password is not valid', () => {
    const passwordVerification = () => {
      const e = createUser('Beatriz','biamarou@gmail.com','123','123');
    };
    expect(passwordVerification).toThrow();
  })

  it('throws an Error beacuse password confirmation fails', () => {
    const passwordConfirmVerification = () => {
      const e = createUser('Beatriz','biamarou@gmail.com','123456','123457');
    };
    expect(passwordConfirmVerification).toThrow();
  })
});
