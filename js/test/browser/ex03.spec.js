const test = require('ava');

function scaffoldStructure(document, data) {
  let ul = document.createElement('ul');

  data.forEach((object) => {
    let li = document.createElement('li');

    let b = document.createElement('b');
    b.appendChild(document.createTextNode(`${object.name}`));
    b.className = 'name';
    b.style.fontWeight = 'bold';

    let u = document.createElement('u');
    u.appendChild(document.createTextNode(`${object.email}`));
    u.className = 'email';
    u.style.textDecoration = 'underline';

    li.appendChild(b);
    li.appendChild(document.createTextNode(' - '));
    li.appendChild(u);
    ul.appendChild(li);
  })
  document.body.appendChild(ul);
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);
  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);
});

test('page has unordered list', t => {
  const unorderedList = document.getElementsByTagName('ul');
  t.is(unorderedList.length > 0, true);
});

test('page has .name class', t => {
  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length > 0, true);
});

test('page content is sorted', t => {
  let sorted = true;
  const nameNodes = document.querySelectorAll('.name');

  for (let index = 1; index < nameNodes.length; index++) {
    if (nameNodes[index].innerHTML < nameNodes[index - 1].innerHTML)
      sorted = false;
  }
  t.is(sorted, true);
});

test('page has required style', t => {
  let style = true;
  const nameStyle = document.querySelector('.name').style;
  const emailStyle = document.querySelector('.email').style;
  if(nameStyle.fontWeight != 'bold'  || emailStyle.textDecoration != 'underline')
    style = false;
  t.is(style, true);
});